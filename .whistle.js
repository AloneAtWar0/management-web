/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

const pkg = require('./package.json');

exports.name = `[${pkg.description}]本地环境配置`;
exports.rules = `
/192.168.1.108:3000/(?!chainmaker)/ proxy://127.0.0.1:3000
`;
