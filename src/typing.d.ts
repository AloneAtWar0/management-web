/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
interface Window {
  __REDUX_DEVTOOLS_EXTENSION__: any;
}

/**
 * @description 具体配置值见Package.json
 */
declare const CHAIN_MAKER: {
  sdkVersion: string;
  subscribeMinSdkVersion: string;
};

declare module 'worker.ts' {
  class MyWorker extends Worker {
    constructor();
  }

  export default MyWorker;
}
