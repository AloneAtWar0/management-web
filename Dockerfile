FROM node:14.17.0 as builder
MAINTAINER alanhe "alanqianghe@tencent.com"

ENV NODE_ENV development

RUN echo " ------------------Web打包 --------------------"

WORKDIR /management-web

COPY . /management-web

RUN npm install
RUN npm run build

RUN echo " ------------------Web容器部署启动 --------------------"

FROM nginx:1.19.2
COPY --from=builder /management-web/build /usr/share/nginx/html
COPY deploy/nginx/conf.d /etc/nginx/conf.d
EXPOSE 80
