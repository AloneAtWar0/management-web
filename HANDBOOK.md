## 环境依赖
- node版本为`>=12.0.0`

  下载地址：https://nodejs.org

  若已安装，请通过命令查看版本：

  ```shell
  $ node -v
  ```

- docker版本为`>=20.10.7`

  docker-compose版本为`>=1.29.2`

  下载地址：
   - https://docs.docker.com/engine/install/
   - https://docs.docker.com/compose/install/

  若已安装，请通过命令查看版本：

  ```shell
  $ docker -v
  $ docker-compose -v
  ```

## 部署

### 代码下载

下载地址：

### 资源打包

```shell
# 安装依赖
$ npm install

# 构建打包
$ npm build
```

### 容器部署

1. 将打包好的`dist`文件夹内容拷贝到目标目标服务器文件夹`management-web`

2. 将仓库下deploy中文件拷贝到目标服务器`management-web`同路径下，修改deploy/nginx/conf.d/default.conf文件中服务端代理地址

   ```
   location /chainmaker {
            # proxy_pass代理地址为对应API服务地址
            proxy_pass http://192.168.1.108:9999;
        }
   ```

3. 在确保服务器已安装`docker`,`docker-compose`的基础上，执行`docker-compose up -d`，启动OK后即可访问
